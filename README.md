# Estudos Gitlab

Treinamento para certificação GitLab Associate.

# AULA 01
```bash
- Entendemos o que é o Git
- Entendemos o que é o Working Dir, Stage/Index e HEAD
- Entendemos o que é o GitLab
- Como criar um grupo no GitLab
- Como criar um repositorio Git
- Aprendemos os comandos basicos para manipulação de arquivos e diretorios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o merge na master/main
```
